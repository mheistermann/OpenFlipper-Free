/*===========================================================================*\
 *                                                                           *
 *                              OpenFlipper                                  *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
\*===========================================================================*/

/*===========================================================================*\
 *                                                                           *
 *   $Revision$                                                      *
 *   $Author$                                                       *
 *   $Date$                   *
 *                                                                           *
\*===========================================================================*/

//================================================================
//
//  CLASS SplatCloudObjectSelectionPlugin
//
//================================================================


#ifndef SPLATCLOUDOBJECTSELECTIONPLUGIN_HH
#define SPLATCLOUDOBJECTSELECTIONPLUGIN_HH


//== INCLUDES ====================================================


#include <QObject>

#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/BackupInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>
#include <OpenFlipper/BasePlugin/SelectionInterface.hh>
#include <OpenFlipper/BasePlugin/LoadSaveInterface.hh>
#include <OpenFlipper/BasePlugin/INIInterface.hh>
#include <OpenFlipper/BasePlugin/KeyInterface.hh>
#include <OpenFlipper/BasePlugin/ScriptInterface.hh>

#include <OpenFlipper/common/Types.hh>
#include <OpenFlipper/BasePlugin/PluginFunctions.hh>
#include <OpenFlipper/INIFile/INIFile.hh>

#include <ObjectTypes/SplatCloud/SplatCloud.hh>


//== CLASS DEFINITION ============================================


class SplatCloudObjectSelectionPlugin : public QObject, BaseInterface, BackupInterface, LoggingInterface, SelectionInterface, LoadSaveInterface, INIInterface, KeyInterface, ScriptInterface
{
  Q_OBJECT
  Q_INTERFACES( BaseInterface      )
  Q_INTERFACES( BackupInterface    )
  Q_INTERFACES( LoggingInterface   )
  Q_INTERFACES( SelectionInterface )
  Q_INTERFACES( LoadSaveInterface  )
  Q_INTERFACES( INIInterface       )
  Q_INTERFACES( KeyInterface       )
  Q_INTERFACES( ScriptInterface    )

#if QT_VERSION >= 0x050000
  Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-SelectionSplatCloudObject")
#endif

public:
  /// Default constructor
  SplatCloudObjectSelectionPlugin() : vertexType_( 0 ), allSupportedTypes_( 0 ) { }

  friend class SelectVolumeAction;

signals:

  //-- Base Interface --
  void updateView();
  void updatedObject( int _objectId, const UpdateType& _type );
  void nodeVisibilityChanged( int _objectId );
  void setSlotDescription( QString _slotName, QString _slotDescription, QStringList _parameters, QStringList _descriptions );

  //-- Backup Interface --
  void createBackup( int _objectId, QString _name, UpdateType _type = UPDATE_ALL );

  //-- Logging Interface --
  void log(                QString _message );
  void log( Logtype _type, QString _message );

  //-- Selection Interface --
  void addSelectionEnvironment( QString _modeName, QString _description, QString _icon, QString &_handleName );
  void registerType( QString _handleName, DataType _type );
  void addPrimitiveType( QString _handleName, QString _name, QString _icon, SelectionInterface::PrimitiveType &_typeHandle );
  void addSelectionOperations( QString _handleName, QStringList _operationsList, QString _category, SelectionInterface::PrimitiveType _type = 0u );

  void showToggleSelectionMode     ( QString _handleName, bool _show, SelectionInterface::PrimitiveType _associatedTypes );
  void showSphereSelectionMode     ( QString _handleName, bool _show, SelectionInterface::PrimitiveType _associatedTypes );
  void showLassoSelectionMode      ( QString _handleName, bool _show, SelectionInterface::PrimitiveType _associatedTypes );
  void showVolumeLassoSelectionMode( QString _handleName, bool _show, SelectionInterface::PrimitiveType _associatedTypes );

  void getActiveDataTypes( SelectionInterface::TypeList &_types );
  void getActivePrimitiveType( SelectionInterface::PrimitiveType &_type );
  void targetObjectsOnly( bool &_targetsOnly );

  void registerKeyShortcut( int _key, Qt::KeyboardModifiers _modifiers = Qt::NoModifier );

  //-- LoadSave Interface --
  void deleteObject( int _objectId );

  //-- Script Interface --
  void scriptInfo( QString _functionName );

public slots:

  //-- Selection Interface --
  void loadSelection( int _objectId, const QString &_filename );

private slots:

  //-- INI Interface --
  void loadIniFile( INIFile &_ini, int _objectId );
  void saveIniFile( INIFile &_ini, int _objectId );

  //-- Base Interface --
  void initializePlugin();
  void pluginsInitialized();
  void noguiSupported() { };

  //-- Selection Interface --
  void slotSelectionOperation( QString _operation );

  void slotToggleSelection     ( QMouseEvent *_event,                 SelectionInterface::PrimitiveType _currentType, bool _deselect );
  void slotSphereSelection     ( QMouseEvent *_event, double _radius, SelectionInterface::PrimitiveType _currentType, bool _deselect );
  void slotLassoSelection      ( QMouseEvent *_event,                 SelectionInterface::PrimitiveType _currentType, bool _deselect );
  void slotVolumeLassoSelection( QMouseEvent *_event,                 SelectionInterface::PrimitiveType _currentType, bool _deselect );

  void slotLoadSelection( const INIFile &_file );
  void slotSaveSelection(       INIFile &_file );

  void slotKeyShortcutEvent( int _key, Qt::KeyboardModifiers _modifiers );

public:

  //-- Base Interface --
  QString name()        { return QString( tr("SplatCloud Object Selection"                 ) ); }
  QString description() { return QString( tr("Allows to select parts of SplatCloud Objects") ); }

  //===========================================================================
  /** @name Private methods
  * @{ */
  //===========================================================================
private:

  /// Set descriptions for local public slots
  void updateSlotDescriptions();

  /// Set color for selection
  void setColorForSelection( const int _objectId, const PrimitiveType _primitiveType );

  /** @} */

  //===========================================================================
  /** @name Private slots
  * @{ */
  //===========================================================================
private slots:

  /** @} */

public slots:

  QString version() { return QString( "1.0" ); }

  // Is vertex type active? (for use in plugins that need SplatCloud selection)
  bool vertexTypeActive()
  {
    SelectionInterface::PrimitiveType t;
    emit getActivePrimitiveType( t );
    return (t & vertexType_) > 0;
  }

  //===========================================================================
  /** @name Scriptable slots
  * @{ */
  //===========================================================================
public slots:

  //==========================================
  // VERTEX OPERATIONS
  //==========================================

  void   selectVertices         ( int _objectId, IdList _vertexList );             //!< Select given vertices
  void   unselectVertices       ( int _objectId, IdList _vertexList );             //!< Unselect given vertices
  void   selectAllVertices      ( int _objectId );                                 //!< Select all vertices
  void   clearVertexSelection   ( int _objectId );                                 //!< Unselect all vertices
  void   invertVertexSelection  ( int _objectId );                                 //!< Invert the current vertex selection
  IdList getVertexSelection     ( int _objectId );                                 //!< Return a list of all selected vertices
  void   deleteVertexSelection  ( int _objectId );                                 //!< Delete vertices that are currently selected
  void   colorizeVertexSelection( int _objectId, int _r, int _g, int _b, int _a ); //!< Colorize the vertex selection

  //===========================================================================

  void lassoSelect( QRegion &_region, PrimitiveType _primitiveType, bool _deselection ); //!< Lasso selection tool

  /** @} */

  //===========================================================================
  /** @name Template Functions
  * @{ */
  //===========================================================================

private:

  bool splatCloudDeleteSelection    ( SplatCloud *_splatCloud,                                                      PrimitiveType _primitiveType                                  ); //!< Delete all selected elements of a SplatCloud
  void splatCloudToggleSelection    ( SplatCloud *_splatCloud, uint _index, ACG::Vec3d &_hit_point,                 PrimitiveType _primitiveType                                  ); //!< Toggle SplatCloud selection
  void splatCloudSphereSelection    ( SplatCloud *_splatCloud, uint _index, ACG::Vec3d &_hit_point, double _radius, PrimitiveType _primitiveTypes, bool _deselection              ); //!< Use the event to paint selection with a sphere
  bool splatCloudVolumeSelection    ( SplatCloud *_splatCloud, ACG::GLState &_state, QRegion *_region,              PrimitiveType _primitiveTypes, bool _deselection              ); //!< Surface volume selection tool
  void splatCloudColorizeSelection  ( SplatCloud *_splatCloud,                                                      PrimitiveType _primitiveTypes, int _r, int _g, int _b, int _a ); //!< Colorize the selection

  /** @} */

  //===========================================================================
  /** @name Member variables
  * @{ */
  //===========================================================================

private:

  QString                           environmentHandle_; //!< Handle to selection environment
  SelectionInterface::PrimitiveType vertexType_;        //!< Primitive type handle
  SelectionInterface::PrimitiveType allSupportedTypes_; //!< Primitive type handle
  QPolygon                          lasso_2Dpoints_;    //!< Used for lasso selection tool
  QVector<QPoint>                   volumeLassoPoints_; //!< Used for volume lasso tool

  /** @} */
};


/// Traverse the scenegraph and call the selection function for all SplatCloud nodes
class SelectVolumeAction
{
public:

  SelectVolumeAction( QRegion &_region, SplatCloudObjectSelectionPlugin *_plugin, unsigned int _type, bool _deselection, ACG::GLState &_state ) : 
    state_      ( _state       ),
    region_     ( _region      ),
    plugin_     ( _plugin      ),
    type_       ( _type        ),
    deselection_( _deselection )
  { }

  void enter( BaseNode * /*_node*/ ) { }
  void leave( BaseNode * /*_node*/ ) { }

  bool operator()( BaseNode *_node );

private:
  ACG::GLState                    &state_;
  QRegion                         &region_;
  SplatCloudObjectSelectionPlugin *plugin_;
  unsigned int                    type_;
  bool                            deselection_;
};


//================================================================


#endif // SPLATCLOUDOBJECTSELECTIONPLUGIN_HH
