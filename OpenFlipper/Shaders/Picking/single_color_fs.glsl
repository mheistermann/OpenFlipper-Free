// picking - all fragments have the same pick id

#version 130

uniform vec4 color;

out vec4 outColor;

void main()
{
  outColor = color;
}