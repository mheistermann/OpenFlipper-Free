/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

/*===========================================================================*\
*                                                                            *
*   $Revision$                                                       *
*   $LastChangedBy$                                                *
*   $Date$                     *
*                                                                            *
\*===========================================================================*/


#define OVM_PROPERTY_VISUALIZER_VECTOR_CC

#include <ACG/Utils/ColorConversion.hh>
#include "OVMPropertyVisualizerVector.hh"


template <typename MeshT>
OVMPropertyVisualizerVector<MeshT>::OVMPropertyVisualizerVector(MeshT* _mesh, int objectID, PropertyInfo _propertyInfo)
    : OVMPropertyVisualizer<MeshT>(_mesh, objectID, _propertyInfo)
{
    if (PropertyVisualizer::widget) delete PropertyVisualizer::widget;
    VectorWidget* w = new VectorWidget();
    w->paramVector->setTitle(QString("3D Vector Parameters of ").append(PropertyVisualizer::propertyInfo.propName().c_str()));
    PropertyVisualizer::widget = w;

    BaseObjectData *bod;
    PluginFunctions::getObject(objectID, bod);
    lineNode = new ACG::SceneGraph::LineNode(ACG::SceneGraph::LineNode::LineSegmentsMode, bod->baseNode());

    w->vectors_edges_rb->hide();
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::clear()
{
    lineNode->clear();
    OVMPropertyVisualizer<MeshT>::clear();
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::duplicateProperty()
{
    OVMPropertyVisualizer<MeshT>::template duplicateProperty_stage1<ACG::Vec3d>();
}

template<typename MeshT>
template<typename PropType, typename EntityIterator>
void OVMPropertyVisualizerVector<MeshT>::visualizeVectorAsColorForEntity(PropType prop, EntityIterator e_begin, EntityIterator e_end) {
    if (!prop)
        throw VizException("Getting PropHandle from mesh for selected property failed.");
    VolumeMeshObject<MeshT>* object;
    PluginFunctions::getObject(OVMPropertyVisualizer<MeshT>::mObjectID, object);
    for (EntityIterator e_it = e_begin; e_it != e_end; ++e_it) {
        ACG::Vec3d v = prop[*e_it].normalized() * .5 + ACG::Vec3d(.5, .5, .5);
        object->colors()[*e_it] = ACG::Vec4f(v[0], v[1], v[2], 1.0);
    }
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::visualizeCellProp(bool _setDrawMode)
{
    VectorWidget* w = (VectorWidget*)PropertyVisualizer::widget;
    if (w->vectors_colors_rb->isChecked())
    {
        OpenVolumeMesh::CellPropertyT<ACG::Vec3d> prop = OVMPropertyVisualizer<MeshT>::mesh->template request_cell_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
        visualizeVectorAsColorForEntity(prop,
                                        OVMPropertyVisualizer<MeshT>::mesh->cells_begin(),
                                        OVMPropertyVisualizer<MeshT>::mesh->cells_end());
        if (_setDrawMode)
        {
            VolumeMeshObject<MeshT>* object;
            PluginFunctions::getObject(OVMPropertyVisualizer<MeshT>::mObjectID, object);
            object->setObjectDrawMode(OVMPropertyVisualizer<MeshT>::drawModes.cellsColoredPerCell);
        }
    }
    else visualizeCellPropAsStrokes();
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::visualizeFaceProp(bool _setDrawMode)
{
    VectorWidget* w = (VectorWidget*)PropertyVisualizer::widget;
    if (w->vectors_colors_rb->isChecked())
    {
        OpenVolumeMesh::FacePropertyT<ACG::Vec3d> prop = OVMPropertyVisualizer<MeshT>::mesh->template request_face_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
        visualizeVectorAsColorForEntity(prop,
                                        OVMPropertyVisualizer<MeshT>::mesh->faces_begin(),
                                        OVMPropertyVisualizer<MeshT>::mesh->faces_end());
        if (_setDrawMode)
        {
            VolumeMeshObject<MeshT>* object;
            PluginFunctions::getObject(OVMPropertyVisualizer<MeshT>::mObjectID, object);
            object->setObjectDrawMode(OVMPropertyVisualizer<MeshT>::drawModes.facesColoredPerFace);
        }
    }
    else visualizeFacePropAsStrokes();
}


template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::visualizeHalffaceProp(bool _setDrawMode)
{
    VectorWidget* w = (VectorWidget*)PropertyVisualizer::widget;
    if (w->vectors_colors_rb->isChecked())
    {
        OpenVolumeMesh::HalfFacePropertyT<ACG::Vec3d> prop = OVMPropertyVisualizer<MeshT>::mesh->template request_halfface_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
        visualizeVectorAsColorForEntity(prop,
                                        OVMPropertyVisualizer<MeshT>::mesh->halffaces_begin(),
                                        OVMPropertyVisualizer<MeshT>::mesh->halffaces_end());
        if (_setDrawMode)
        {
            VolumeMeshObject<MeshT>* object;
            PluginFunctions::getObject(OVMPropertyVisualizer<MeshT>::mObjectID, object);
            object->setObjectDrawMode(OVMPropertyVisualizer<MeshT>::drawModes.halffacesColoredPerHalfface);
        }
    }
    else visualizeHalffacePropAsStrokes();
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::visualizeEdgeProp(bool _setDrawMode)
{
    VectorWidget* w = (VectorWidget*)PropertyVisualizer::widget;
    if (w->vectors_colors_rb->isChecked())
    {
        OpenVolumeMesh::EdgePropertyT<ACG::Vec3d> prop = OVMPropertyVisualizer<MeshT>::mesh->template request_edge_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
        visualizeVectorAsColorForEntity(prop,
                                        OVMPropertyVisualizer<MeshT>::mesh->edges_begin(),
                                        OVMPropertyVisualizer<MeshT>::mesh->edges_end());
        if (_setDrawMode)
        {
            VolumeMeshObject<MeshT>* object;
            PluginFunctions::getObject(OVMPropertyVisualizer<MeshT>::mObjectID, object);
            object->setObjectDrawMode(OVMPropertyVisualizer<MeshT>::drawModes.edgesColoredPerEdge);
        }
    }
    else visualizeEdgePropAsStrokes();
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::visualizeHalfedgeProp(bool _setDrawMode)
{
    VectorWidget* w = (VectorWidget*)PropertyVisualizer::widget;
    if (w->vectors_colors_rb->isChecked())
    {
        OpenVolumeMesh::HalfEdgePropertyT<ACG::Vec3d> prop = OVMPropertyVisualizer<MeshT>::mesh->template request_halfedge_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
        visualizeVectorAsColorForEntity(prop,
                                        OVMPropertyVisualizer<MeshT>::mesh->halfedges_begin(),
                                        OVMPropertyVisualizer<MeshT>::mesh->halfedges_end());
        if (_setDrawMode)
        {
            VolumeMeshObject<MeshT>* object;
            PluginFunctions::getObject(OVMPropertyVisualizer<MeshT>::mObjectID, object);
            object->setObjectDrawMode(OVMPropertyVisualizer<MeshT>::drawModes.halfedgesColoredPerHalfedge);
        }
    }
    else visualizeHalfedgePropAsStrokes();
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::visualizeVertexProp(bool _setDrawMode)
{
    VectorWidget* w = (VectorWidget*)PropertyVisualizer::widget;
    if (w->vectors_colors_rb->isChecked())
    {
        OpenVolumeMesh::VertexPropertyT<ACG::Vec3d> prop = OVMPropertyVisualizer<MeshT>::mesh->template request_vertex_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
        visualizeVectorAsColorForEntity(prop,
                                        OVMPropertyVisualizer<MeshT>::mesh->vertices_begin(),
                                        OVMPropertyVisualizer<MeshT>::mesh->vertices_end());
        if (_setDrawMode)
        {
            VolumeMeshObject<MeshT>* object;
            PluginFunctions::getObject(OVMPropertyVisualizer<MeshT>::mObjectID, object);
            object->setObjectDrawMode(OVMPropertyVisualizer<MeshT>::drawModes.verticesColored);
        }
    }
    else visualizeVertexPropAsStrokes();
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::visualizeFacePropAsStrokes()
{
    VectorWidget* vectorWidget = static_cast<VectorWidget*>(PropertyVisualizer::widget);

    lineNode->clear();

    ACG::Vec4f color = ACG::to_Vec4f(vectorWidget->lineColor->color());

    OpenVolumeMesh::FacePropertyT<ACG::Vec3d> prop = OVMPropertyVisualizer<MeshT>::mesh->template request_face_property<ACG::Vec3d>(PropertyVisualizer::propertyInfo.propName());

    if ( !prop )
        return;

    OpenVolumeMesh::FaceIter f_begin(OVMPropertyVisualizer<MeshT>::mesh->faces_begin()), f_end(OVMPropertyVisualizer<MeshT>::mesh->faces_end());
    for (OpenVolumeMesh::FaceIter f_it = f_begin; f_it != f_end; ++f_it){

        ACG::Vec3d center(0.0, 0.0, 0.0);
        int vCount = 0;

        OpenVolumeMesh::HalfFaceHandle hfh = OVMPropertyVisualizer<MeshT>::mesh->halfface_handle(*f_it, 0);
        for (OpenVolumeMesh::HalfFaceVertexIter hfv_it = OVMPropertyVisualizer<MeshT>::mesh->hfv_iter(hfh); hfv_it; ++hfv_it){
            vCount++;
            center += OVMPropertyVisualizer<MeshT>::mesh->vertex(*hfv_it);
        }

        center /= vCount;

        ACG::Vec3d v  = prop[*f_it];

        if (vectorWidget->normalize->isChecked() && v.sqrnorm() > 1e-12)
            v.normalize();

        if(vectorWidget->scale->isChecked())
            v *= vectorWidget->scaleBox->value();

        lineNode->add_line( center, (center+v) );
        lineNode->add_color(color);
    }
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::visualizeEdgePropAsStrokes()
{
    VectorWidget* vectorWidget = static_cast<VectorWidget*>(PropertyVisualizer::widget);

    lineNode->clear();

    ACG::Vec4f color = ACG::to_Vec4f(vectorWidget->lineColor->color());

    OpenVolumeMesh::EdgePropertyT<ACG::Vec3d> prop = OVMPropertyVisualizer<MeshT>::mesh->template request_edge_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
    if ( !prop )
        return;

    OpenVolumeMesh::EdgeIter e_begin(OVMPropertyVisualizer<MeshT>::mesh->edges_begin()), e_end(OVMPropertyVisualizer<MeshT>::mesh->edges_end());
    for (OpenVolumeMesh::EdgeIter e_it = e_begin; e_it != e_end; ++e_it){

        OpenVolumeMesh::OpenVolumeMeshEdge edge = OVMPropertyVisualizer<MeshT>::mesh->edge(*e_it);
        ACG::Vec3d v1 = OVMPropertyVisualizer<MeshT>::mesh->vertex(edge.from_vertex());
        ACG::Vec3d v2 = OVMPropertyVisualizer<MeshT>::mesh->vertex(edge.to_vertex());
        ACG::Vec3d start = 0.5*(v1+v2);

        ACG::Vec3d v  = prop[*e_it];

        if (vectorWidget->normalize->isChecked() && v.sqrnorm() > 1e-12)
            v.normalize();



        if(vectorWidget->scale->isChecked())
        v *= vectorWidget->scaleBox->value();

        lineNode->add_line( start, (start+v) );
        lineNode->add_color(color);
    }
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::visualizeHalfedgePropAsStrokes()
{
    VectorWidget* vectorWidget = static_cast<VectorWidget*>(PropertyVisualizer::widget);

    lineNode->clear();

    ACG::Vec4f color = ACG::to_Vec4f(vectorWidget->lineColor->color());

    OpenVolumeMesh::HalfEdgePropertyT<ACG::Vec3d> prop = OVMPropertyVisualizer<MeshT>::mesh->template request_halfedge_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
    if ( !prop )
        return;

    OpenVolumeMesh::HalfEdgeIter he_begin(OVMPropertyVisualizer<MeshT>::mesh->halfedges_begin()), he_end(OVMPropertyVisualizer<MeshT>::mesh->halfedges_end());
    for (OpenVolumeMesh::HalfEdgeIter he_it = he_begin; he_it != he_end; ++he_it){

        OpenVolumeMesh::OpenVolumeMeshEdge edge = OVMPropertyVisualizer<MeshT>::mesh->halfedge(*he_it);

        ACG::Vec3d v1 = OVMPropertyVisualizer<MeshT>::mesh->vertex(edge.from_vertex());
        ACG::Vec3d v2 = OVMPropertyVisualizer<MeshT>::mesh->vertex(edge.to_vertex());
        ACG::Vec3d start = (2.0*v1+v2)/3.0;

        ACG::Vec3d v  = prop[*he_it];

        if (vectorWidget->normalize->isChecked() && v.sqrnorm() > 1e-12)
            v.normalize();

        if(vectorWidget->scale->isChecked())
        v *= vectorWidget->scaleBox->value();

        lineNode->add_line( start, (start+v) );
        lineNode->add_color(color);
    }
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::visualizeVertexPropAsStrokes()
{
    VectorWidget* vectorWidget = static_cast<VectorWidget*>(PropertyVisualizer::widget);

    lineNode->clear();

    ACG::Vec4f color = ACG::to_Vec4f(vectorWidget->lineColor->color());

    OpenVolumeMesh::VertexPropertyT<ACG::Vec3d> prop = OVMPropertyVisualizer<MeshT>::mesh->template request_vertex_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
    if ( !prop )
        return;

    OpenVolumeMesh::VertexIter v_begin(OVMPropertyVisualizer<MeshT>::mesh->vertices_begin()), v_end(OVMPropertyVisualizer<MeshT>::mesh->vertices_end());
    for (OpenVolumeMesh::VertexIter v_it = v_begin; v_it != v_end; ++v_it){

        ACG::Vec3d start = OVMPropertyVisualizer<MeshT>::mesh->vertex(*v_it);

        ACG::Vec3d v  = prop[*v_it];

        if (vectorWidget->normalize->isChecked() && v.sqrnorm() > 1e-12)
            v.normalize();

        if(vectorWidget->scale->isChecked())
        v *= vectorWidget->scaleBox->value();

        lineNode->add_line( start, (start+v) );
        lineNode->add_color(color);
    }
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::visualizeCellPropAsStrokes()
{
    VectorWidget* vectorWidget = static_cast<VectorWidget*>(PropertyVisualizer::widget);

    lineNode->clear();

    ACG::Vec4f color = ACG::to_Vec4f(vectorWidget->lineColor->color());

    OpenVolumeMesh::CellPropertyT<ACG::Vec3d> prop = OVMPropertyVisualizer<MeshT>::mesh->template request_cell_property<ACG::Vec3d>(PropertyVisualizer::propertyInfo.propName());

    if ( !prop )
        return;

    OpenVolumeMesh::CellIter c_begin(OVMPropertyVisualizer<MeshT>::mesh->cells_begin()), c_end(OVMPropertyVisualizer<MeshT>::mesh->cells_end());
    for (OpenVolumeMesh::CellIter c_it = c_begin; c_it != c_end; ++c_it){

        // Compute cell's center
        ACG::Vec3d center(0.0, 0.0, 0.0);
        unsigned int vCount = OVMPropertyVisualizer<MeshT>::mesh->n_vertices_in_cell(*c_it);
        for(OpenVolumeMesh::CellVertexIter cv_it = OVMPropertyVisualizer<MeshT>::mesh->cv_iter(*c_it); cv_it.valid(); ++cv_it) {
            center += OVMPropertyVisualizer<MeshT>::mesh->vertex(*cv_it) / (double)vCount;
        }

        ACG::Vec3d v  = prop[*c_it];

        if (vectorWidget->normalize->isChecked() && v.sqrnorm() > 1e-12)
            v.normalize();

        if(vectorWidget->scale->isChecked())
            v *= vectorWidget->scaleBox->value();

        lineNode->add_line( center, (center+v) );
        lineNode->add_color(color);
    }
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::visualizeHalffacePropAsStrokes()
{
    VectorWidget* vectorWidget = static_cast<VectorWidget*>(PropertyVisualizer::widget);

    lineNode->clear();

    ACG::Vec4f color = ACG::to_Vec4f(vectorWidget->lineColor->color());

    OpenVolumeMesh::HalfFacePropertyT<ACG::Vec3d> prop = OVMPropertyVisualizer<MeshT>::mesh->template request_halfface_property<ACG::Vec3d>(PropertyVisualizer::propertyInfo.propName());

    if ( !prop )
        return;

    OpenVolumeMesh::HalfFaceIter hf_begin(OVMPropertyVisualizer<MeshT>::mesh->halffaces_begin()), hf_end(OVMPropertyVisualizer<MeshT>::mesh->halffaces_end());
    for (OpenVolumeMesh::HalfFaceIter hf_it = hf_begin; hf_it != hf_end; ++hf_it){

        ACG::Vec3d center(0.0, 0.0, 0.0);
        int vCount = 0;

        for (OpenVolumeMesh::HalfFaceVertexIter hfv_it = OVMPropertyVisualizer<MeshT>::mesh->hfv_iter(*hf_it); hfv_it; ++hfv_it){
            vCount++;
            center += OVMPropertyVisualizer<MeshT>::mesh->vertex(*hfv_it);
        }

        center /= vCount;

        ACG::Vec3d v  = prop[*hf_it];

        if (vectorWidget->normalize->isChecked() && v.sqrnorm() > 1e-12)
            v.normalize();

        if(vectorWidget->scale->isChecked())
            v *= vectorWidget->scaleBox->value();

        lineNode->add_line( center, (center+v) );
        lineNode->add_color(color);
    }
}

template <typename MeshT>
QString OVMPropertyVisualizerVector<MeshT>::getPropertyText(unsigned int index)
{
    return OVMPropertyVisualizer<MeshT>::template getPropertyText_<ACG::Vec3d>(index);
}


template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::setCellPropertyFromText(unsigned int index, QString text)
{
    MeshT* mesh = OVMPropertyVisualizer<MeshT>::mesh;

    OpenVolumeMesh::CellPropertyT<ACG::Vec3d> prop = mesh->template request_cell_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
    if ( !prop )
    {
        emit this->log(LOGERR, QObject::tr("Error: No property with name ").append(PropertyVisualizer::propertyInfo.propName().c_str()));
        return;
    }

    OpenVolumeMesh::CellHandle ch(index);

    prop[ch] = this->strToVec3d(text);
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::setFacePropertyFromText(unsigned int index, QString text)
{
    MeshT* mesh = OVMPropertyVisualizer<MeshT>::mesh;

    OpenVolumeMesh::FacePropertyT<ACG::Vec3d> prop = mesh->template request_face_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
    if ( !prop )
    {
        emit this->log(LOGERR, QObject::tr("Error: No property with name ").append(PropertyVisualizer::propertyInfo.propName().c_str()));
        return;
    }

    OpenVolumeMesh::FaceHandle fh(index);

    prop[fh] = this->strToVec3d(text);
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::setHalffacePropertyFromText(unsigned int index, QString text)
{
    MeshT* mesh = OVMPropertyVisualizer<MeshT>::mesh;

    OpenVolumeMesh::HalfFacePropertyT<ACG::Vec3d> prop = mesh->template request_halfface_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
    if ( !prop )
    {
        emit this->log(LOGERR, QObject::tr("Error: No property with name ").append(PropertyVisualizer::propertyInfo.propName().c_str()));
        return;
    }

    OpenVolumeMesh::HalfFaceHandle hfh(index);

    prop[hfh] = this->strToVec3d(text);
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::setEdgePropertyFromText(unsigned int index, QString text)
{
    MeshT* mesh = OVMPropertyVisualizer<MeshT>::mesh;

    OpenVolumeMesh::EdgePropertyT<ACG::Vec3d> prop = mesh->template request_edge_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
    if ( !prop )
    {
        emit this->log(LOGERR, QObject::tr("Error: No property with name ").append(PropertyVisualizer::propertyInfo.propName().c_str()));
        return;
    }

    OpenVolumeMesh::EdgeHandle eh(index);

    prop[eh] = this->strToVec3d(text);
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::setHalfedgePropertyFromText(unsigned int index, QString text)
{
    MeshT* mesh = OVMPropertyVisualizer<MeshT>::mesh;

    OpenVolumeMesh::HalfEdgePropertyT<ACG::Vec3d> prop = mesh->template request_halfedge_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
    if ( !prop )
    {
        emit this->log(LOGERR, QObject::tr("Error: No property with name ").append(PropertyVisualizer::propertyInfo.propName().c_str()));
        return;
    }

    OpenVolumeMesh::HalfEdgeHandle heh(index);

    prop[heh] = this->strToVec3d(text);
}

template <typename MeshT>
void OVMPropertyVisualizerVector<MeshT>::setVertexPropertyFromText(unsigned int index, QString text)
{
    MeshT* mesh = OVMPropertyVisualizer<MeshT>::mesh;

    OpenVolumeMesh::VertexPropertyT<ACG::Vec3d> prop = mesh->template request_vertex_property<ACG::Vec3d>(OVMPropertyVisualizer<MeshT>::propertyInfo.propName());
    if ( !prop )
    {
        emit this->log(LOGERR, QObject::tr("Error: No property with name ").append(PropertyVisualizer::propertyInfo.propName().c_str()));
        return;
    }

    OpenVolumeMesh::VertexHandle vh(index);

    prop[vh] = this->strToVec3d(text);
}

