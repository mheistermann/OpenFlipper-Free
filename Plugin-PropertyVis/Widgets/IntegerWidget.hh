/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

/*===========================================================================*\
*                                                                            *
*   $Revision$                                                       *
*   $LastChangedBy$                                                *
*   $Date$                     *
*                                                                            *
\*===========================================================================*/

#ifndef INTEGER_WIDGET_HH
#define INTEGER_WIDGET_HH

#include "ui_IntegerWidget.hh"

#include "ACG/Utils/IColorCoder.hh"
#include "ACG/Utils/ColorCoder.hh"
#include "ACG/Utils/LinearTwoColorCoder.hh"
#include <ACG/Utils/ColorConversion.hh>
#include <ACG/Utils/SmartPointer.hh>

#if QT_VERSION >= 0x050000 
  #include <QtWidgets>
#else
  #include <QtGui>
#endif


class IntegerWidget : public QWidget, public Ui::IntegerWidget
{
  Q_OBJECT

public:
  IntegerWidget(QWidget * parent = 0)
    : QWidget(parent)
  {
    setupUi(this);
  }

  /**
   * @brief Builds a color coder according to UI settings
   * @return unique_ptr to an IColorCoder for parameters in [0..1]
   */
  std::unique_ptr<ACG::IColorCoder> buildColorCoder()
  {
      if (intColorCoder->isChecked()) {
          return ptr::make_unique<ACG::ColorCoder>();
      } else {
          return ptr::make_unique<ACG::LinearTwoColorCoder>(
                      ACG::to_Vec4f(intMin->color()),
                      ACG::to_Vec4f(intMax->color()));
      }
  }

  
  signals:
    void widgetShown();
        
  protected:
    void showEvent ( QShowEvent* /*event*/ ) {
      emit widgetShown();
    }
};

#endif // INTEGER_WIDGET_HH
