/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

/*===========================================================================*\
*                                                                            *
*   $Revision$                                                       *
*   $LastChangedBy$                                                *
*   $Date$                     *
*                                                                            *
\*===========================================================================*/

#include "HoleFillerPlugin.hh"

#include <MeshTools/MeshSelectionT.hh>

#if QT_VERSION >= 0x050000
#else
#include <QtGui>
#endif

#include "HoleInfoT.hh"

#define HOLEINFO "HoleInfoData"

/// Constructor
HoleFillerPlugin::HoleFillerPlugin() :
       tool_(0)
{

}

/// Initialize the toolbox widget
void HoleFillerPlugin::initializePlugin()
{
   tool_ = new HoleFillerToolbarWidget();
   QSize size(300, 300);
   tool_->resize(size);

   connect(tool_->tableWidget,SIGNAL(itemSelectionChanged()),this,SLOT(slotItemSelectionChanged()));
   
   connect(tool_->tableWidget,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(slotCellDoubleClicked(int,int)));

   connect(tool_->detect,SIGNAL(clicked()),this,SLOT(detectButton()));
   connect(tool_->fillButton, SIGNAL(clicked()), this, SLOT(slotFillSelection()) );

   QIcon* toolIcon = new QIcon(OpenFlipper::Options::iconDirStr()+OpenFlipper::Options::dirSeparator()+"holefilling.png");

   emit addToolbox( tr("Hole Filler") , tool_ , toolIcon);
} 

/// add PickModes after initialization
void HoleFillerPlugin::pluginsInitialized()
{
   emit addPickMode("Separator");
   emit addPickMode("Hole Filler");

   emit setSlotDescription("fillAllHoles(int)", tr("Fill all holes from a given Mesh"),
                           QString("objectId").split(","), QString("Id of the mesh").split(","));

   emit setSlotDescription("fillHole(int,int)", tr("Fill a holes from a given Mesh where edgeHandle is on the boundary"),
                           QString("objectId,edgeHandle").split(","), QString("Id of the mesh,Handle of one boundary edge of the hole").split(","));

   //build the tableWidget with headers
   update_menu();
}

void HoleFillerPlugin::getSelectedHoles(std::vector<int>& _holeIds, std::vector<int>& _objIds)
{
  QModelIndexList indices = tool_->tableWidget->selectionModel()->selectedRows();

  //get a map from objectID to (selected) holeIDs
  for (int i=0; i < indices.size(); i++){
    int objID  = holeMapping_[ indices[i].row() ].first;
    int holeID = holeMapping_[ indices[i].row() ].second;

    _holeIds.push_back  ( holeID );
    _objIds.push_back( objID );
  }
}

/// Fill all selected holes
void HoleFillerPlugin::slotFillSelection(){

  //get a map from objectID to (selected) holeIDs
  std::vector< int > holes;
  std::vector< int > objects;
  getSelectedHoles(holes,objects);

  //init progressDialog
  QProgressDialog progress(tr("Filling holes..."), tr("Abort"), 0, holes.size(), 0);
  progress.setWindowModality(Qt::ApplicationModal);
  progress.setValue(0);

  int counter = 0;

  //iterate over all objects with holes that should be filled
  for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::ALL_OBJECTS) ; o_it != PluginFunctions::objectsEnd(); ++o_it) {

    // TYPE is TRIMESH
    if ( o_it->dataType( DATA_TRIANGLE_MESH ) ) {

      TriMesh* mesh = PluginFunctions::triMesh(o_it);

      //get perObjectData
      HoleInfo< TriMesh >* holeInfo = dynamic_cast< HoleInfo< TriMesh >* > ( o_it->objectData(HOLEINFO) );
    
      if (holeInfo == 0){
        holeInfo = new HoleInfo< TriMesh >( mesh );
        o_it->setObjectData(HOLEINFO, holeInfo);
      }

      //fill the holes
      for (uint i = 0; i < objects.size(); i++)
        if ( objects[i] == o_it->id() ){
          holeInfo->fillHole( holes[i]);

          if (progress.wasCanceled())
            break;
          else
            progress.setValue(++counter);
        }

      //update the object
      emit updatedObject(o_it->id(),UPDATE_ALL);
    
      holeInfo->getHoles();
    
      update_menu();
      emit createBackup( o_it->id(), "Hole Filling", UPDATE_GEOMETRY | UPDATE_TOPOLOGY | UPDATE_SELECTION);
    }
    // DATATYPE is  POLYMESH
    else if ( o_it->dataType( DATA_POLY_MESH ) ) {
      emit log(LOGWARN, tr("HoleFilling unsupported for poly meshes") );
      continue;
    }
    
    //abort if user wants to
    if (progress.wasCanceled())
      break;
  }

  progress.close();

  emit updateView();
}

/// slot for displaying selected holes
void HoleFillerPlugin::slotItemSelectionChanged() {

  std::vector< int > holes;
  std::vector< int > objects;
  getSelectedHoles(holes,objects);

  //iterate over all objects with holes that should be displayed
  for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::ALL_OBJECTS) ; o_it != PluginFunctions::objectsEnd(); ++o_it) {

    // TYPE is TRIMESH
    if ( o_it->dataType( DATA_TRIANGLE_MESH ) ) {

      TriMeshObject* object = PluginFunctions::triMeshObject(*o_it);
      TriMesh* mesh = PluginFunctions::triMesh(o_it);

      //get perObjectData
      HoleInfo< TriMesh >* holeInfo = dynamic_cast< HoleInfo< TriMesh >* > ( o_it->objectData(HOLEINFO) );
    
      if (holeInfo == 0){
        holeInfo = new HoleInfo< TriMesh >( mesh );
        o_it->setObjectData(HOLEINFO, holeInfo);
      }

      //clear the edge selection
      MeshSelection::clearEdgeSelection(mesh);

      //select the holes
      for (uint i = 0; i < objects.size(); i++)
        if ( objects[i] == o_it->id() )
          holeInfo->selectHole( holes[i] );

      // We only fly if we have exacly one object and one hole
      if (  (objects.size() == 1) && (holes.size() == 1) && ( objects[0] == o_it->id() ) ){

        TriMesh::Point center;
        TriMesh::Normal normal;
        holeInfo->getHolePostitionInfo(holes[0], normal, center);

        // Get bounding box to get a scaling for the movement
        TriMesh::Point _bbMin;
        TriMesh::Point _bbMax;

        object->boundingBox(_bbMin, _bbMax);

        PluginFunctions::flyTo(center + normal * (_bbMax-_bbMin).length() , center, 10.0);
      }

      //update the object
      emit updatedObject(o_it->id(),UPDATE_SELECTION);
    }
    // DATATYPE is  POLYMESH
    else if ( o_it->dataType( DATA_POLY_MESH ) ) {

      PolyMeshObject* object = PluginFunctions::polyMeshObject(*o_it);
      PolyMesh* mesh = PluginFunctions::polyMesh(o_it);

      //get perObjectData
      HoleInfo< PolyMesh >* holeInfo = dynamic_cast< HoleInfo< PolyMesh >* > ( o_it->objectData(HOLEINFO) );
    
      if (holeInfo == 0){
        holeInfo = new HoleInfo< PolyMesh >( mesh );
        o_it->setObjectData(HOLEINFO, holeInfo);
      }

      //clear the edge selection
      MeshSelection::clearEdgeSelection(mesh);

      //select the holes
      for (uint i = 0; i < objects.size(); i++)
        if ( objects[i] == o_it->id() )
          holeInfo->selectHole( holes[i] );

      // We only fly if we have exacly one object and one hole
      if (  (objects.size() == 1) && (holes.size() == 1) && ( objects[0] == o_it->id() ) ){

        PolyMesh::Point center;
        PolyMesh::Normal normal;
        holeInfo->getHolePostitionInfo(holes[0], normal, center);

        // Get bounding box to get a scaling for the movement
        PolyMesh::Point _bbMin;
        PolyMesh::Point _bbMax;

        object->boundingBox(_bbMin, _bbMax);

        PluginFunctions::flyTo(center + normal * (_bbMax-_bbMin).length() , center, 10.0);
      }

      //update the object
      emit updatedObject(o_it->id(),UPDATE_SELECTION);

    }
  }

  emit updateView();
}

/// Slot for filling holes from double-clicked rows
void HoleFillerPlugin::slotCellDoubleClicked(int _row , int /*_col*/) {

  if ( _row > (int)holeMapping_.size() ) {
    emit log(LOGWARN, tr("Error for holeMapping_ vector size") );
    return;
  }

  BaseObjectData* object;
  int objID  = holeMapping_[_row].first;
  int holeID = holeMapping_[_row].second;

  if ( !PluginFunctions::getObject( objID, object ) ) {
    emit log(LOGWARN, tr("Unable to find object for hole (should not happen!!)") );
    return;
  }

  // TYPE is TRIMESH
  if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

    TriMesh* mesh = PluginFunctions::triMesh(object);

    //get perObjectData
    HoleInfo< TriMesh >* holeInfo = dynamic_cast< HoleInfo< TriMesh >* > ( object->objectData(HOLEINFO) );
  
    if (holeInfo == 0){
      holeInfo = new HoleInfo< TriMesh >( mesh );
      object->setObjectData(HOLEINFO, holeInfo);
    }

    //fill the hole
    holeInfo->fillHole( holeID );

    emit updatedObject(object->id(),UPDATE_ALL);

    holeInfo->getHoles();

    update_menu();

    emit updateView();
  }
  // DATATYPE is  POLYMESH
  else if ( object->dataType( DATA_POLY_MESH ) ) {
    
    emit log(LOGWARN,tr("HoleFilling unsupported for poly meshes"));

    return;
  }
}

/// detect holes on all objects
void HoleFillerPlugin::detectButton( )
{
  for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::ALL_OBJECTS) ; o_it != PluginFunctions::objectsEnd(); ++o_it) {

    //case TRIMESH
    if ( o_it->dataType( DATA_TRIANGLE_MESH ) ) {

      HoleInfo< TriMesh >* holeInfo = dynamic_cast< HoleInfo< TriMesh >* > ( o_it->objectData(HOLEINFO) );
    
      if ( holeInfo == 0 ){
        TriMesh* mesh = PluginFunctions::triMesh(*o_it);
        holeInfo = new HoleInfo< TriMesh >( mesh );
        o_it->setObjectData(HOLEINFO, holeInfo);
      }

      holeInfo->getHoles();
    }

     //case POLYMESH
     if ( o_it->dataType( DATA_POLY_MESH ) ) {

       HoleInfo< PolyMesh >* holeInfo = dynamic_cast< HoleInfo< PolyMesh >* > ( o_it->objectData(HOLEINFO) );

       if (holeInfo == 0){
         PolyMesh* mesh = PluginFunctions::polyMesh(*o_it);
         holeInfo = new HoleInfo< PolyMesh >( mesh );
         o_it->setObjectData(HOLEINFO, holeInfo);
       }

       holeInfo->getHoles();
     }
  }

  update_menu();
}

/// check for holes if an object has changed
void HoleFillerPlugin::slotObjectUpdated( int _identifier, const UpdateType& _type ) {

  BaseObjectData* object;

  // Check if this is a usable Object
  if ( !PluginFunctions::getObject(_identifier,object) ) 
    return;

  bool updated = false;
  
  if ( _type.contains(UPDATE_TOPOLOGY) ) {

    // get holes for TRIMESH
    if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

      HoleInfo< TriMesh >* holeInfo = dynamic_cast< HoleInfo< TriMesh >* > ( object->objectData(HOLEINFO) );

      if ( holeInfo ) {
        holeInfo->getHoles();
        updated = true;
      }
    }

    // get holes for POLYMESH
    else if ( object->dataType( DATA_POLY_MESH ) ) {

      HoleInfo< PolyMesh >* holeInfo = dynamic_cast< HoleInfo< PolyMesh >* > ( object->objectData(HOLEINFO) );

      if ( holeInfo ) {
        holeInfo->getHoles();
        updated = true;
      }
    }
  }

  // Only update if something has changed!
  if ( updated )
    update_menu();
}

///update the entries in the tableWidget
void HoleFillerPlugin::update_menu() {

  holeMapping_.clear();

  tool_->tableWidget->clear();

  tool_->tableWidget->setRowCount ( 0 );
  tool_->tableWidget->setColumnCount ( 4 );

  QStringList headerdata;
  headerdata << "Object" << "Edges" << "Boundary Length" << "BB Diagonal";

  tool_->tableWidget->setHorizontalHeaderLabels(headerdata);
  tool_->updateGeometry();
  
  int elements = 0;
  int count = 0;

  //iterate over all objects
  for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::ALL_OBJECTS) ; o_it != PluginFunctions::objectsEnd(); ++o_it) {

    // DATATYPE is  TRIMESH
    if ( o_it->dataType( DATA_TRIANGLE_MESH ) ) {

      //get perObjectData  
      HoleInfo< TriMesh >* holeInfo = dynamic_cast< HoleInfo< TriMesh >* > ( o_it->objectData(HOLEINFO) );

      if (holeInfo != 0){
        elements += holeInfo->holes()->size();
        tool_->tableWidget->setRowCount ( elements );

        //add holes to the table
        for (uint i = 0 ; i < holeInfo->holes()->size() ; ++i ) {
          // Set Name of the object
          QTableWidgetItem* name = new QTableWidgetItem( o_it->name() );
          name->setFlags( 0 );
          name->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled);
          tool_->tableWidget->setItem(count,0,name);

          size_t egde_count = 0;
          double boundaryLength = 0.0;
          TriMesh::Scalar bbDiagonal = 0.0;

          holeInfo->getHoleInfo(i, egde_count, boundaryLength, bbDiagonal);

          // Set Number of the edges
          QTableWidgetItem* size = new QTableWidgetItem( QString::number( egde_count ) );
          size->setFlags( 0 );
          size->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled);
          tool_->tableWidget->setItem(count,1,size);
    
          // Set boundary Length
          QTableWidgetItem* boundaryLengthWidget = new QTableWidgetItem( QString::number(boundaryLength) );
          boundaryLengthWidget->setFlags( 0 );
          boundaryLengthWidget->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled);
          tool_->tableWidget->setItem(count,2,boundaryLengthWidget);

          QTableWidgetItem* bbDiagonalWidget = new QTableWidgetItem( QString::number(bbDiagonal) );
          bbDiagonalWidget->setFlags( 0 );
          bbDiagonalWidget->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled);
          tool_->tableWidget->setItem(count,3,bbDiagonalWidget);

          // remember the id for the hole
          holeMapping_.push_back( std::pair<int , int>( o_it->id() , i ) );
    
          ++count;
        }
      }
    }
    // DATATYPE is  POLYMESH
    else if ( o_it->dataType( DATA_POLY_MESH ) ) {

      //get perObjectData  
      HoleInfo< PolyMesh >* holeInfo = dynamic_cast< HoleInfo< PolyMesh >* > ( o_it->objectData(HOLEINFO) );

      if (holeInfo != 0){
        elements += holeInfo->holes()->size();
        tool_->tableWidget->setRowCount ( elements );

        //add holes to the table
        for (uint i = 0 ; i < holeInfo->holes()->size() ; ++i ) {
          // Set Name of the object
          QTableWidgetItem* name = new QTableWidgetItem( o_it->name() );
          name->setFlags( 0 );
          name->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled);
          tool_->tableWidget->setItem(count,0,name);

          size_t egde_count = 0;
          double boundaryLength = 0.0;
          TriMesh::Scalar bbDiagonal = 0.0;

          holeInfo->getHoleInfo(i, egde_count, boundaryLength, bbDiagonal);

          // Set Number of the edges
          QTableWidgetItem* size = new QTableWidgetItem( QString::number( egde_count ) );
          size->setFlags( 0 );
          size->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled);
          tool_->tableWidget->setItem(count,1,size);
    
          // Set Bounding box diagonal
          QTableWidgetItem* radius = new QTableWidgetItem( QString::number(boundaryLength) );
          radius->setFlags( 0 );
          radius->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled);
          tool_->tableWidget->setItem(count,2,radius);

          // Set Bounding box diagonal
          QTableWidgetItem* bbDiagonalWidget = new QTableWidgetItem( QString::number(bbDiagonal) );
          bbDiagonalWidget->setFlags( 0 );
          bbDiagonalWidget->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled);
          tool_->tableWidget->setItem(count,3,bbDiagonalWidget);
    
          // remember the id for the hole
          holeMapping_.push_back( std::pair<int , int>( o_it->id() , i ) );
    
          ++count;
        }
      }
    }
  }

  tool_->tableWidget->resizeColumnToContents ( 1 );
}

/// fill all holes from a given object
void HoleFillerPlugin::fillAllHoles(int _objectID){
  BaseObjectData* object = 0;
  PluginFunctions::getObject( _objectID , object );

  if (object == 0){
    emit log(LOGERR, tr("Could not get object from ID.") );
    return;
  }
  
  emit scriptInfo( "fillAllHoles( ObjectId )"  );


  // TYPE is TRIMESH
  if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

    TriMesh* mesh = PluginFunctions::triMesh(object);

    //get perObjectData
    HoleInfo< TriMesh >* holeInfo = dynamic_cast< HoleInfo< TriMesh >* > ( object->objectData(HOLEINFO) );
  
    if (holeInfo == 0){
      holeInfo = new HoleInfo< TriMesh >( mesh );
      object->setObjectData(HOLEINFO, holeInfo);
    }

    //fill the hole
    holeInfo->fillAllHoles();

    //and update everything
    emit updatedObject(object->id(),UPDATE_ALL);

    holeInfo->getHoles();

    update_menu();

    emit updateView();
  }
  // DATATYPE is  POLYMESH
  else if ( object->dataType( DATA_POLY_MESH ) ) {
    emit log(LOGERR, tr("HoleFilling unsopported for poly meshes.") );
    return;
  }
}


/// fill a hole in given object where _edgeHandle is on the boundary
void HoleFillerPlugin::fillHole(int _objectID, int _edgeHandle){
  BaseObjectData* object = 0;
  PluginFunctions::getObject( _objectID , object );

  if (object == 0){
    emit log(LOGERR, tr("Could not get object from ID.") );
    return;
  }
  
  emit scriptInfo( "fillHole( ObjectId , EdgeHandle )"  );


  // TYPE is TRIMESH
  if ( object->dataType( DATA_TRIANGLE_MESH ) ) {

    TriMesh* mesh = PluginFunctions::triMesh(object);

    //get perObjectData
    HoleInfo< TriMesh >* holeInfo = dynamic_cast< HoleInfo< TriMesh >* > ( object->objectData(HOLEINFO) );
  
    if (holeInfo == 0){
      holeInfo = new HoleInfo< TriMesh >( mesh );
      object->setObjectData(HOLEINFO, holeInfo);
    }

    //check edgeHandle
    TriMesh::EdgeHandle eh(_edgeHandle);

    if ( !eh.is_valid() || !mesh->is_boundary(eh) ){
      emit log(LOGERR, tr("Invalid edge handle.") );
      return;
    }

    //fill the hole
    holeInfo->fillHole( eh );

    //and update everything
    emit updatedObject(object->id(),UPDATE_ALL);

    holeInfo->getHoles();

    update_menu();

    emit updateView();
  }
  // DATATYPE is  POLYMESH
  else if ( object->dataType( DATA_POLY_MESH ) ) {
    emit log(LOGERR, tr("HoleFilling unsopported for poly meshes.") );
    return;
  }
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2( holefillerplugin , HoleFillerPlugin );
#endif

